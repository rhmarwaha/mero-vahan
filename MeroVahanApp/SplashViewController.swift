//
//  SplashViewController.swift
//  MeroVahanApp
//
//  Created by Rohit Marwaha on 23/10/19.
//  Copyright © 2019 Rohit Marwaha. All rights reserved.
//

import UIKit

class SplashViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        self.navigationController?.navigationBar.isHidden = true
        self.navigationController?.navigationBar.tintColor = UIColor(named: "lightThemeColor")
        
        
        if let loginViewController = self.storyboard?.instantiateViewController(withIdentifier: "LoginViewController") as? LoginViewController {
            self.navigationController?.pushViewController(loginViewController, animated: true)
        }
    }
    override func viewWillAppear(_ animated: Bool) {
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        sleep(3)
    }
}
