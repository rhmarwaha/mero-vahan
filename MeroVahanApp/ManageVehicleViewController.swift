//
//  ManageVehicleViewController.swift
//  MeroVahanApp
//
//  Created by Rohit Marwaha on 31/10/19.
//  Copyright © 2019 Rohit Marwaha. All rights reserved.
//

import UIKit

class ManageVehicleViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let yourBackImage = #imageLiteral(resourceName: "leftarrow")
        self.navigationController?.navigationBar.backIndicatorImage = yourBackImage
        self.navigationController?.navigationBar.backIndicatorTransitionMaskImage = yourBackImage
        self.navigationItem.title = ""
        
        self.navigationController?.navigationBar.isHidden = false
        self.navigationController?.navigationBar.barTintColor = UIColor(named: "lightThemeColor")
        self.navigationController?.navigationBar.tintColor = .white
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor : UIColor.white]
        self.title = "Manage Vehicles"
        
        
        loadTableView()
    }
    
    func loadTableView(){
        let nibCell = UINib(nibName: "ManageVehicleTableViewCell", bundle: Bundle.main)
        tableView.register(nibCell, forCellReuseIdentifier: "ManageVehicleTableViewCell")
        tableView.delegate = self
        tableView.dataSource = self
         tableView.showsVerticalScrollIndicator = false
    }
    

    @IBAction func addVehicleButtonAction(_ sender: Any) {
        if let addVehicleViewController = self.storyboard?.instantiateViewController(withIdentifier: "AddVehicleViewController") as? AddVehicleViewController{
            self.navigationController?.pushViewController(addVehicleViewController, animated: true)
        }
    }
    
}

extension ManageVehicleViewController: UITableViewDelegate, UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 10
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard  let cell = tableView.dequeueReusableCell(withIdentifier: "ManageVehicleTableViewCell", for: indexPath) as? ManageVehicleTableViewCell else {
            fatalError("Cell does not load")
        }
        return cell
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 10.0
    }
    
}
