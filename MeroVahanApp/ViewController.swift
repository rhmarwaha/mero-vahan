//
//  ViewController.swift
//  MeroVahanApp
//
//  Created by Rohit Marwaha on 23/10/19.
//  Copyright © 2019 Rohit Marwaha. All rights reserved.
//

import UIKit


class ViewController: UIViewController {
    

    @IBOutlet weak var firstNameTextField: UITextField!
    @IBOutlet weak var lastNameTextField: UITextField!
    @IBOutlet weak var emailAddressTextFiled: UITextField!
    @IBOutlet weak var countryCodeTextField: UITextField!
    @IBOutlet weak var mobileNumberTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var confirmPasswordTextField: UITextField!
    @IBOutlet weak var referralCodeTextField: UITextField!
    @IBOutlet weak var checkBoxButton: UIButton!
    @IBOutlet weak var alreadyLoginLabel: UILabel!
    @IBOutlet weak var termsAndConditionLabel: UILabel!
    @IBOutlet weak var registerButton: UIButton!
    @IBOutlet weak var alreadyLoginView: UIView!
    
    
    var isCheckedCheckbox: Bool = false
    let countryCodePickerView = UIPickerView()
    
    let countriesCode = ["+91","+977"]
    let countriesName = ["India","Nepal"]
    
    let MAX_LENGTH_PHONENUMBER = 10
    let ACCEPTABLE_NUMBERS     = "0123456789"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        addAccessories()
    }
    
    func addAccessories(){
        
        let yourBackImage = #imageLiteral(resourceName: "leftarrow")
        self.navigationController?.navigationBar.backIndicatorImage = yourBackImage
        self.navigationController?.navigationBar.backIndicatorTransitionMaskImage = yourBackImage
        self.navigationItem.title = ""
        
        self.navigationController?.navigationBar.isHidden = false
        self.navigationController?.navigationBar.barTintColor = UIColor(named: "lightThemeColor")
        self.navigationController?.navigationBar.tintColor = .white
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor : UIColor.white]
        self.title = "Register"
        
        self.hideKeyboardWhenTappedAround()
        
        let registerString:NSString = "Already have an account? Log in"
        var myMutableString = NSMutableAttributedString()
        myMutableString = NSMutableAttributedString(string: registerString as String, attributes: [NSAttributedString.Key.font:UIFont(name: "Georgia", size: 18.0)!])
        myMutableString.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor(named: "lightThemeColor")!, range: NSRange(location:25, length:6))
        alreadyLoginLabel.attributedText = myMutableString
        
        let termsString: NSString = "I agree to the Terms & Services"
        var termsMutableString = NSMutableAttributedString()
        termsMutableString = NSMutableAttributedString(string: termsString as String, attributes: [NSAttributedString.Key.font:UIFont(name: "Georgia", size: 15.0)!])
        termsMutableString.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor(named: "lightThemeColor")!, range: NSRange(location:15, length:16))
        termsAndConditionLabel.attributedText = termsMutableString
        
        registerButton.layer.shadowColor = UIColor.black.cgColor
        registerButton.layer.shadowOffset = CGSize(width: 10, height: 10)
        registerButton.layer.shadowRadius = 5
        registerButton.layer.shadowOpacity = 0.2
        
        passwordTextField.addRightView()
        confirmPasswordTextField.addRightView()
        
        let toolBar = UIToolbar()
        toolBar.barStyle = UIBarStyle.default
        toolBar.isTranslucent = true
        toolBar.tintColor = .blue
        toolBar.sizeToFit()
        let doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItem.Style.done, target: self, action: #selector(tapDone))
        
        toolBar.setItems([doneButton], animated: true)
        toolBar.isUserInteractionEnabled = true
        firstNameTextField.inputAccessoryView = toolBar
        lastNameTextField.inputAccessoryView = toolBar
        emailAddressTextFiled.inputAccessoryView = toolBar
        countryCodeTextField.inputAccessoryView = toolBar
        mobileNumberTextField.inputAccessoryView = toolBar
        passwordTextField.inputAccessoryView = toolBar
        confirmPasswordTextField.inputAccessoryView = toolBar
        referralCodeTextField.inputAccessoryView = toolBar
        
        firstNameTextField.delegate = self
        lastNameTextField.delegate = self
        mobileNumberTextField.delegate = self
        mobileNumberTextField.keyboardType = .phonePad
        
        countryCodeTextField.inputView = countryCodePickerView
        countryCodePickerView.delegate = self
        countryCodePickerView.dataSource = self
        
        termsAndConditionLabel.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(openTermsAndConditionWebpage)))
        termsAndConditionLabel.isUserInteractionEnabled = true
        
        alreadyLoginView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(openLoginViewController)))
    }
    
    @objc func openLoginViewController(){
        self.navigationController?.popViewController(animated: true)
    }
    @objc
    func tapDone(){
        self.view.endEditing(true)
    }
    @objc func openTermsAndConditionWebpage(){
        if let termsConditionViewController = self.storyboard?.instantiateViewController(withIdentifier: "TermsConditionViewController") as? TermsConditionViewController {
            self.navigationController?.pushViewController(termsConditionViewController, animated: true)
        }
    }
    @IBAction func registerButtonAction(_ sender: Any) {
        let firstNameValue = firstNameTextField.text ?? ""
        let lastNameValue = lastNameTextField.text ?? ""
        let emailValue = emailAddressTextFiled.text ?? ""
        let countryCodeValue = countryCodeTextField.text ?? ""
        let mobileNumberValue = mobileNumberTextField.text ?? ""
        let passwordValue = passwordTextField.text ?? ""
        let confirmPasswordValue = confirmPasswordTextField.text ?? ""
        let referralValue = referralCodeTextField.text ?? ""
        if firstNameValue.isEmpty {
            alertControlling(title: "First Name Warning", message: "First Name is missing")
        }
        if lastNameValue.isEmpty{
            alertControlling(title: "Last Name Warning", message: "Last Name is missing")
        }
        if !emailValue.isValidEmail(){
            alertControlling(title: "Email Warning", message: "Email Pattern is Wrong")
        }
        if countryCodeValue.isEmpty{
            alertControlling(title: "Country Code Warning", message: "Country Code is missing")
        }
        if mobileNumberValue.isEmpty || !(mobileNumberValue.count == 10){
            alertControlling(title: "Mobile Number Warning", message: "Please enter 10 digits mobile number")
        }
        if passwordValue.isEmpty{
            alertControlling(title: "Password Warning", message: "Password is missing")
        }
        if confirmPasswordValue.isEmpty{
            alertControlling(title: "Confirm Password Warning", message: "Confirm Paswword is missing")
        }
        if !passwordValue.elementsEqual(confirmPasswordValue){
            alertControlling(title: "Mismatch Warning", message: "Passwords are mismatched")
        }
        if !passwordValue.isValidPassword(){
            alertControlling(title: "Password Warning", message: "Password must contain 1 numeric, 1 symbol, 1 capital letter and more than 7 characters")
        }
        if !isCheckedCheckbox {
            alertControlling(title: "Terms and Condition", message: "Please accept terms and condition")
        }
        
        let user = User(firstName: firstNameValue, lastName: lastNameValue, emailAddress: emailValue, countryCode: countryCodeValue, mobileNumber: mobileNumberValue, password: passwordValue)
        
        //Api hit
        
        ApiHit.signUpApiHit(user: user) { (isSuccess, response) in
            if isSuccess {
                
                guard let data = response as? [String : Any] else{
                    return
                }
                print(data)
                
                guard let status = data["status"] as? Int else {
                    return
                }
                print(status)
               
                
                if status == 200 {
                    
                    if let accountCreatedViewController = self.storyboard?.instantiateViewController(withIdentifier: "AccountCreatedViewController") as? AccountCreatedViewController {
                        self.navigationController?.pushViewController(accountCreatedViewController, animated: true)
                    }
                    
                    return
                }
               
                guard let message = data["message"]  as? String else {
                    return
                }
                self.alertControlling(title: "Register Problem", message: message)
                return
            
            }
            self.alertControlling(title: "Network Error", message: "Network problem in you area")
        }
        
        
        if let accountCreatedViewController = self.storyboard?.instantiateViewController(withIdentifier: "AccountCreatedViewController") as? AccountCreatedViewController {
            self.navigationController?.pushViewController(accountCreatedViewController, animated: true)
        }
        

    }
    func  alertControlling(title: String, message: String)  {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let okAction = UIAlertAction(title: "Ok", style: .default) { (_) in
            alertController.dismiss(animated: true, completion: nil)
        }
        alertController.addAction(okAction)
        self.present(alertController, animated: true)
    }
    
    @IBAction func checkbox(_ sender: Any) {
        if isCheckedCheckbox {
            checkBoxButton.setImage(#imageLiteral(resourceName: "unfilledCheckbox"), for: .normal)
            isCheckedCheckbox = false
            return
        }
        checkBoxButton.setImage(#imageLiteral(resourceName: "filledCheckbox"), for: .normal)
        isCheckedCheckbox = true
    }
}

extension ViewController: UITextFieldDelegate{
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if textField == self.mobileNumberTextField{
            let newLength: Int = textField.text?.count ?? 0 + string.count - range.length
            let numberOnly = NSCharacterSet.init(charactersIn: ACCEPTABLE_NUMBERS).inverted
            let strValid = string.rangeOfCharacter(from: numberOnly) == nil
            
            if let char = string.cString(using: String.Encoding.utf8) {
                let isBackSpace = strcmp(char, "\\b")
                if (isBackSpace == -92) {
                    return true
                }
            }
            
            
            return (strValid && (newLength < self.MAX_LENGTH_PHONENUMBER))
        }
        
        let allowedCharacters = CharacterSet.letters
        let characterSet = CharacterSet(charactersIn: string)
        return allowedCharacters.isSuperset(of: characterSet)
    }
}

extension ViewController: UIPickerViewDelegate, UIPickerViewDataSource{
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return countriesCode.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
            return countriesName[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        countryCodeTextField.text = countriesCode[row]
    }
}
extension String {
    func isValidEmail() -> Bool {
        // here, `try!` will always succeed because the pattern is valid
        let regex = try! NSRegularExpression(pattern: "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$", options: .caseInsensitive)
        return regex.firstMatch(in: self, options: [], range: NSRange(location: 0, length: count)) != nil
    }
    func isValidPassword() -> Bool {
        let passwordRegex = "^(?=.*\\d)(?=.*[a-z])(?=.*[A-Z])[0-9a-zA-Z!@#$%^&*()\\-_=+{}|?>.<,:;~`’]{8,}$"
        return NSPredicate(format: "SELF MATCHES %@", passwordRegex).evaluate(with: self)
    }
}
