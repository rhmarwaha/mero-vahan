//
//  CustomAlertViewController.swift
//  MeroVahanApp
//
//  Created by Rohit Marwaha on 30/10/19.
//  Copyright © 2019 Rohit Marwaha. All rights reserved.
//

import UIKit

class CustomAlertViewController: UIViewController {

    @IBOutlet weak var noRemoveLabel: UILabel!
    @IBOutlet weak var removeButtonOutlet: UIButton!
    @IBOutlet weak var contentView: UIView!
    
    var deleteRowDelegate: DeleteRowDelegate?
    var rowToDelete: Int?
    override func viewDidLoad() {
        super.viewDidLoad()
        
        contentView.layer.cornerRadius = 10.0
        
        removeButtonOutlet.layer.shadowColor = UIColor.black.cgColor
        removeButtonOutlet.layer.shadowOffset = CGSize(width: 10, height: 10)
        removeButtonOutlet.layer.shadowRadius = 5
        removeButtonOutlet.layer.shadowOpacity = 0.2
        
        let tapRecognizer = UITapGestureRecognizer(target: self, action: #selector(noRemveAction))
        noRemoveLabel.addGestureRecognizer(tapRecognizer)
        noRemoveLabel.isUserInteractionEnabled = true
    }
    
    
    @objc
    func noRemveAction(){
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func removeButton(_ sender: Any) {
        deleteRowDelegate?.deleteRow(row: rowToDelete ?? 0)
        self.dismiss(animated: true, completion: nil)
    }
}
