//
//  HomePageViewController.swift
//  MeroVahanApp
//
//  Created by Rohit Marwaha on 25/10/19.
//  Copyright © 2019 Rohit Marwaha. All rights reserved.
//

import UIKit

class HomePageViewController: UIViewController {

    @IBOutlet weak var manageResourceView: UIView!
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var sideMenuButton: UIButton!
    var resources = [HomeResources]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loadTableView()
        addAccesories()
        loadData()
        
        
        sideMenuButton.addTarget(self, action: #selector(SSASideMenu.presentLeftMenuViewController), for: .touchUpInside)
       
    }
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = true
    }
    
    func loadData(){
        resources = [HomeResources(name: "Drivers", image: #imageLiteral(resourceName: "deliveryMan")), HomeResources(name: "Vehicle", image: #imageLiteral(resourceName: "deliveryVan")), HomeResources(name: "Status", image: #imageLiteral(resourceName: "status"))]
    }
   
    
    func addAccesories(){
        
        
        manageResourceView.layer.shadowColor = UIColor.black.cgColor
        manageResourceView.layer.shadowOffset = CGSize(width: 7, height: 7)
        manageResourceView.layer.shadowRadius = 5
        manageResourceView.layer.shadowOpacity = 0.1
    }
    
    func loadTableView(){
        let nibCell1 = UINib(nibName: "HomePageTableViewCell", bundle: Bundle.main)
        tableView.register(nibCell1, forCellReuseIdentifier: "HomePageTableViewCell")
        let nibCell2 = UINib(nibName: "HomePage2TableViewCell", bundle: Bundle.main)
        tableView.register(nibCell2, forCellReuseIdentifier: "HomePage2TableViewCell")
        
        tableView.delegate = self
        tableView.dataSource = self
        
        tableView.showsVerticalScrollIndicator = false
    }
}

extension HomePageViewController: UITableViewDelegate, UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return resources.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.row % 2 == 0{
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "HomePageTableViewCell", for: indexPath) as? HomePageTableViewCell else{
                fatalError("cell does not load")
            }
            cell.cellImage.image = resources[indexPath.row].image
            cell.cellName.text = resources[indexPath.row].name
            return cell
        }
        
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "HomePage2TableViewCell", for: indexPath) as? HomePage2TableViewCell else{
            fatalError("cell does not load")
        }
        cell.cellName.text = resources[indexPath.row].name
        cell.imageCell.image = resources[indexPath.row].image
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        switch indexPath.row {
            
            
        case 0:
            if let manageDriverViewController = self.storyboard?.instantiateViewController(withIdentifier: "ManageDriverViewController") as? ManageDriverViewController{
               self.navigationController?.pushViewController(manageDriverViewController, animated: true)
            }
            break
            
            
        case 1:
            
            if let manageVehicleViewController = self.storyboard?.instantiateViewController(withIdentifier: "ManageVehicleViewController") as? ManageVehicleViewController{
                self.navigationController?.pushViewController(manageVehicleViewController, animated: true)
            }
            
            break
        case 2:
            break
        default:
            break
        }
        
    }
}
