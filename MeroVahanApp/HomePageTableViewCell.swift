//
//  HomePageTableViewCell.swift
//  MeroVahanApp
//
//  Created by Rohit Marwaha on 25/10/19.
//  Copyright © 2019 Rohit Marwaha. All rights reserved.
//

import UIKit

class HomePageTableViewCell: UITableViewCell {

    @IBOutlet weak var cellImage: UIImageView!
    @IBOutlet weak var cellName: UILabel!
    @IBOutlet weak var cellView: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        setUpView()
        
    }

    func setUpView(){
        
        cellImage.layer.cornerRadius = 50.0
        
        cellImage.layer.shadowColor = UIColor.red.cgColor
        cellImage.layer.shadowOffset = CGSize(width: 10, height: 10)
        cellImage.layer.shadowRadius = 5
        cellImage.layer.shadowOpacity = 0.4
        cellImage.layoutIfNeeded()
        
        cellView.layer.shadowColor = UIColor.black.cgColor
        cellView.layer.shadowOffset = CGSize(width: 10, height: 10)
        cellView.layer.shadowRadius = 5
        cellView.layer.shadowOpacity = 0.1
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
    
}
