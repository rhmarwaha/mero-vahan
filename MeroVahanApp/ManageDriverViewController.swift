//
//  ManageDriverViewController.swift
//  MeroVahanApp
//
//  Created by Rohit Marwaha on 29/10/19.
//  Copyright © 2019 Rohit Marwaha. All rights reserved.
//

import UIKit

class ManageDriverViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        let yourBackImage = #imageLiteral(resourceName: "leftarrow")
        self.navigationController?.navigationBar.backIndicatorImage = yourBackImage
        self.navigationController?.navigationBar.backIndicatorTransitionMaskImage = yourBackImage
        self.navigationItem.title = ""
        
        self.navigationController?.navigationBar.isHidden = false
        self.navigationController?.navigationBar.barTintColor = UIColor(named: "lightThemeColor")
        self.navigationController?.navigationBar.tintColor = .white
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor : UIColor.white]
        self.title = "Manage Drivers"

        loadTableView()
    }
    
    func loadTableView(){
        let nibCell = UINib(nibName: "ManageDriverTableViewCell", bundle: Bundle.main)
        tableView.register(nibCell, forCellReuseIdentifier: "ManageDriverTableViewCell")
        tableView.delegate = self
        tableView.dataSource = self
        tableView.showsVerticalScrollIndicator = false
    }
    @IBAction func addDriverButtonAction(_ sender: Any) {
        
        if let addDriverViewController = self.storyboard?.instantiateViewController(withIdentifier: "AddDriverViewController") as? AddDriverViewController {
            self.navigationController?.pushViewController(addDriverViewController, animated: true)
        }
        
    }
}
extension ManageDriverViewController: UITableViewDelegate, UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 10
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "ManageDriverTableViewCell") as? ManageDriverTableViewCell else{
            fatalError("Cell does not load")
        }
        cell.deleteButtonDelegate = self
        cell.deleteButton.tag = indexPath.section
        //print("\(cell.deleteButton.tag)")
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 10.0
    }
    
}


protocol DeleteButtonDelegate{
    func deleteButtonTapped(row: Int)
}
protocol DeleteRowDelegate{
    func deleteRow(row: Int)
}
extension ManageDriverViewController: DeleteButtonDelegate, DeleteRowDelegate{
    func deleteButtonTapped(row: Int) {
        let customAlertViewController = CustomAlertViewController(nibName: "CustomAlertViewController", bundle: Bundle.main)
        customAlertViewController.deleteRowDelegate = self
        customAlertViewController.rowToDelete = row
        customAlertViewController.modalPresentationStyle = .overCurrentContext
        self.present(customAlertViewController,animated: true)
    }
    
    func deleteRow(row: Int) {
        // array delete
        print("\(row)")
        tableView.reloadData()
    }
    
  
}
