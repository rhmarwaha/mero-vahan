//
//  AddDriverViewController.swift
//  MeroVahanApp
//
//  Created by Rohit Marwaha on 30/10/19.
//  Copyright © 2019 Rohit Marwaha. All rights reserved.
//

import UIKit

class AddDriverViewController: UIViewController {
    
    
    @IBOutlet weak var fullNameTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var countryCodeTextField: UITextField!
    @IBOutlet weak var mobhileNumberTextField: UITextField!
    @IBOutlet weak var licenseNumberTextField: UITextField!
    @IBOutlet weak var licenseExpiryDateTextField: UITextField!
    @IBOutlet weak var vehicleNumberTextField: UITextField!
    
    @IBOutlet weak var driverProfileImageView: UIImageView!
    @IBOutlet weak var addDriverButtonOutlet: UIButton!
    
    let MAX_LENGTH_PHONENUMBER = 10
    let ACCEPTABLE_NUMBERS     = "0123456789"
    
    let countryCodePickerView = UIPickerView()
    let datePicker = UIDatePicker()
    var imagePickerController = UIImagePickerController()
    
    let countriesCode = ["+91","+977"]
    let countriesName = ["India","Nepal"]
    
    override func viewDidLoad() {
        super.viewDidLoad()

        loadAccessories()
        
        
    }
    @IBAction func chooseImage(_ sender: Any) {
        let actionSheet = UIAlertController(title: "Photo Source", message: "Choose A Source", preferredStyle: .actionSheet)
        let galleryAction = UIAlertAction(title: "Gallery", style: .default) { (_) in
            if UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.savedPhotosAlbum){
                self.imagePickerController.allowsEditing = true
                self.imagePickerController.sourceType = UIImagePickerController.SourceType.savedPhotosAlbum
                self.present(self.imagePickerController,animated: true)
            }
        }
        let cameraAction = UIAlertAction(title: "camera", style: .default) { (_) in
            if UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.camera){
                self.imagePickerController.allowsEditing = true
                self.imagePickerController.sourceType = UIImagePickerController.SourceType.camera
                self.present(self.imagePickerController,animated: true)
            }
        }
        
        actionSheet.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        
        actionSheet.addAction(galleryAction)
        actionSheet.addAction(cameraAction)
        
        
        self.present(actionSheet,animated: true)
        
    }
    
    func loadAccessories(){
        
        driverProfileImageView.layer.cornerRadius = 50.0
        let yourBackImage = #imageLiteral(resourceName: "leftarrow")
        self.navigationController?.navigationBar.backIndicatorImage = yourBackImage
        self.navigationController?.navigationBar.backIndicatorTransitionMaskImage = yourBackImage
        self.navigationItem.title = ""
        
        self.navigationController?.navigationBar.isHidden = false
        self.navigationController?.navigationBar.barTintColor = UIColor(named: "lightThemeColor")
        self.navigationController?.navigationBar.tintColor = .white
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor : UIColor.white]
        self.title = "Add Driver"
        
        
        addDriverButtonOutlet.layer.shadowColor = UIColor.black.cgColor
        addDriverButtonOutlet.layer.shadowOffset = CGSize(width: 10, height: 10)
        addDriverButtonOutlet.layer.shadowRadius = 5
        addDriverButtonOutlet.layer.shadowOpacity = 0.2
        
        self.hideKeyboardWhenTappedAround()
        
        licenseExpiryDateTextField.addRightCalendarView()
        vehicleNumberTextField.addRightDropDownView()
        
        fullNameTextField.delegate = self
        mobhileNumberTextField.delegate = self
        licenseNumberTextField.delegate = self
        mobhileNumberTextField.keyboardType = .phonePad
        
        imagePickerController.delegate = self
        
        countryCodeTextField.inputView = countryCodePickerView
        countryCodePickerView.delegate = self
        countryCodePickerView.dataSource = self
        
        datePicker.datePickerMode = .date
        datePicker.minimumDate = Calendar.current.date(byAdding: Calendar.Component.year, value: -20, to: Date())
       // datePicker.maximumDate = Calendar.current.date(byAdding: Calendar.Component.year , value: -13, to: Date())
        datePicker.addTarget(self, action: #selector(returnDataValue), for: UIControl.Event.valueChanged)
        licenseExpiryDateTextField.inputView = datePicker

    }
    @objc func returnDataValue(){
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd-MM-yyyy"
        licenseExpiryDateTextField.text = dateFormatter.string(from: datePicker.date)
        
    }

    @IBAction func addDriverAction(_ sender: Any) {
        let fullNameValue = fullNameTextField.text ?? ""
        let emailAddressvalue = emailTextField.text ?? ""
        let countryCodeValue = countryCodeTextField.text ?? ""
        let phoneNumberValue = mobhileNumberTextField.text ?? ""
        let licenseNumberValue = licenseNumberTextField.text ?? ""
        let licenseExpiryDateValue = licenseExpiryDateTextField.text ?? ""
        let vehicleNumberValue = vehicleNumberTextField.text ?? ""
        
        if fullNameValue.isEmpty{
            alertControlling(title: "Full Name Warning", message: "Full Name is Missing ......")
        }
        if emailAddressvalue.isEmpty || !emailAddressvalue.isValidEmail(){
            alertControlling(title: "Email Warning", message: "Not a valid email ........")
        }
        if countryCodeValue.isEmpty{
            alertControlling(title: "Country Code Warning", message: "Country code is missing ....")
        }
        if phoneNumberValue.isEmpty || !(phoneNumberValue.count == 10){
            alertControlling(title: "Mobile Number Warning", message: "Mobile Number must be of 10 characters")
        }
        if licenseNumberValue.isEmpty{
            alertControlling(title: "License Number Warning", message: "License NumBer Is Missing .......")
        }
        if licenseExpiryDateValue.isEmpty{
            alertControlling(title: "License Expiry Date Value Warning", message: "License Expiry Date is Missing ......")
        }
        if vehicleNumberValue.isEmpty{
            alertControlling(title: "Vehicle Number Warning ", message: "Vehicle Number is missing .....")
        }
        
        
    }
    func  alertControlling(title: String, message: String)  {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let okAction = UIAlertAction(title: "Ok", style: .default) { (_) in
            alertController.dismiss(animated: true, completion: nil)
        }
        alertController.addAction(okAction)
        self.present(alertController, animated: true)
    }
}

extension UITextField{
    func addRightCalendarView(){
        let btnView = UIButton(frame: CGRect(x: 0, y: 0, width: ((self.frame.height) * 0.95), height: ((self.frame.height) * 0.95)))
        btnView.setImage(#imageLiteral(resourceName: "calendar"), for: .normal)
        btnView.imageEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        self.rightViewMode = .always
        self.rightView = btnView
        btnView.addTarget(self, action: #selector(showCalendar(sender:)), for: UIControl.Event.touchUpInside)
    }
    
    @objc
    func showCalendar(sender: UIButton){
        print("pressed calendar")
        self.becomeFirstResponder()
    }
    
    func addRightDropDownView(){
        let btnView = UIButton(frame: CGRect(x: 0, y: 0, width: ((self.frame.height) * 0.95), height: ((self.frame.height) * 0.95)))
        btnView.setImage(#imageLiteral(resourceName: "dropdown"), for: .normal)
        btnView.imageEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        self.rightViewMode = .always
        self.rightView = btnView
        btnView.addTarget(self, action: #selector(dropDown(sender:)), for: UIControl.Event.touchUpInside)
    }
    @objc
    func dropDown(sender: UIButton){
        print("pressed drop down")
    }
}


extension AddDriverViewController: UITextFieldDelegate{
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if textField == self.mobhileNumberTextField{
            let newLength: Int = textField.text?.count ?? 0 + string.count - range.length
            let numberOnly = NSCharacterSet.init(charactersIn: ACCEPTABLE_NUMBERS).inverted
            let strValid = string.rangeOfCharacter(from: numberOnly) == nil
            
            if let char = string.cString(using: String.Encoding.utf8) {
                let isBackSpace = strcmp(char, "\\b")
                if (isBackSpace == -92) {
                    return true
                }
            }
            return (strValid && (newLength < self.MAX_LENGTH_PHONENUMBER))
        }
        
        if textField == self.licenseNumberTextField{
            
            let allowedCharacters = CharacterSet(charactersIn: " ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789")
            let characterSet = CharacterSet(charactersIn: string)
            return allowedCharacters.isSuperset(of: characterSet)
            
        }
        
        
        let allowedCharacters = CharacterSet.letters
        let characterSet = CharacterSet(charactersIn: string)
        return allowedCharacters.isSuperset(of: characterSet)
    }
}
extension AddDriverViewController: UIPickerViewDelegate, UIPickerViewDataSource{
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return countriesCode.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return countriesName[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        countryCodeTextField.text = countriesCode[row]
    }
}

extension AddDriverViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate{
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        self.dismiss(animated: true, completion: nil)
        
        guard let image = (info[UIImagePickerControllerOriginalImage] as? UIImage) else{
            return
        }
        
        driverProfileImageView.image = image
    }
    
}
