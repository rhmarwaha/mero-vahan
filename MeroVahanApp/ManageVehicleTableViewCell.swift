//
//  ManageVehicleTableViewCell.swift
//  MeroVahanApp
//
//  Created by Rohit Marwaha on 31/10/19.
//  Copyright © 2019 Rohit Marwaha. All rights reserved.
//

import UIKit

class ManageVehicleTableViewCell: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.contentView.layer.borderColor = UIColor.lightGray.cgColor
        self.contentView.layer.borderWidth = 1.0
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

       
    }
    
}
