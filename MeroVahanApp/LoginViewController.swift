//
//  LoginViewController.swift
//  MeroVahanApp
//
//  Created by Rohit Marwaha on 23/10/19.
//  Copyright © 2019 Rohit Marwaha. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController {

    @IBOutlet weak var logInButton: UIButton!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var registerLabel: UILabel!
    @IBOutlet weak var registerViewLabel: UIView!
    @IBOutlet weak var emailTextfield: UITextField!
    @IBOutlet weak var countryCodeTextField: UITextField!
    
    let MAX_LENGTH_PHONENUMBER = 10
    let ACCEPTABLE_NUMBERS     = "0123456789"
    
    let countryCodePickerView = UIPickerView()
    
    let countriesCode = ["+91","+977"]
    let countriesName = ["India","Nepal"]
    
    override func viewDidLoad() {
        super.viewDidLoad()

        addAccesories()
    }
    
    func addAccesories(){
        
        
        self.hideKeyboardWhenTappedAround()
        
        self.navigationController?.navigationBar.barTintColor = UIColor(named: "lightThemeColor")
        self.navigationController?.navigationBar.tintColor = UIColor(named: "lightThemeColor")
        self.navigationItem.title = ""
        
        let registerString:NSString = "Don't have an account? Register"
        var myMutableString = NSMutableAttributedString()
        myMutableString = NSMutableAttributedString(string: registerString as String, attributes: [NSAttributedString.Key.font:UIFont(name: "Georgia", size: 15.0)])
        myMutableString.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor(named: "lightThemeColor"), range: NSRange(location:23, length:8))
        registerLabel.attributedText = myMutableString
        
        logInButton.layer.shadowColor = UIColor.black.cgColor
        logInButton.layer.shadowOffset = CGSize(width: 10, height: 10)
        logInButton.layer.shadowRadius = 5
        logInButton.layer.shadowOpacity = 0.2
        
        passwordTextField.addRightView()
        
        
        let toolBar = UIToolbar()
        toolBar.barStyle = UIBarStyle.default
        toolBar.isTranslucent = true
        toolBar.tintColor = .blue
        toolBar.sizeToFit()
        let doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItem.Style.done, target: self, action: #selector(tapDone))
        
        toolBar.setItems([doneButton], animated: true)
        toolBar.isUserInteractionEnabled = true
        countryCodeTextField.inputAccessoryView = toolBar
        countryCodeTextField.inputView = countryCodePickerView
        countryCodePickerView.delegate = self
        countryCodePickerView.dataSource = self
        emailTextfield.keyboardType = .phonePad
        emailTextfield.inputAccessoryView = toolBar
        emailTextfield.delegate = self
        
        registerViewLabel.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(openSignUpController)))
    }
    
    
    @objc
    func tapDone(){
        self.view.endEditing(true)
    }
    @IBAction func loginButtonAction(_ sender: Any) {
        let emailValue = emailTextfield.text ?? ""
        let passwordValue = passwordTextField.text ?? ""
        let countryCodeValue = countryCodeTextField.text ?? ""
        
        if emailValue.isEmpty || !(emailValue.count == 10) {
            alertControlling(title: "Mobile Number Warning", message: "Mobile number must be of 10 digits ")
        }
        if passwordValue.isEmpty {
            alertControlling(title: "Password Warning", message: "Password is missing...")
        }
        if countryCodeValue.isEmpty{
            alertControlling(title: "Country Code", message: "Country Code is missing")
        }
        
        ApiHit.loginApiHit(countryCode: countryCodeValue, mobile: emailValue, password: passwordValue) { (isSuccess, response) in
            
            guard let data = response as? [String : Any] else{
                return
            }
            print(data)
            guard let status = data["statusCode"] as? Int else {
                return
            }

            
            if isSuccess {
            
               // print(status)
                
                guard let dataValue = data["data"] as? [String: Any] else{
                    return
                }
                guard let token = dataValue["token"] as? String else{
                    return
                }
                print(token)
                
                if status == 200 {
                  //  self.alertControlling(title: "Logined", message: "Login Successfully")
                    
                    // save access token to user defaults
                    UserDefaults.standard.set(token, forKey: UserDefaultsKeys.token)
                    
                    //Home View Controller
                    if let homePageViewController = self.storyboard?.instantiateViewController(withIdentifier: "SSASideMenu") as? SSASideMenu {
                        self.navigationController?.pushViewController(homePageViewController, animated: true)
                    }
                    
                    
                    return
                }
               
            }
            guard let message = data["message"]  as? String else {
                return
            }
            self.alertControlling(title: "Login Problem", message: message)
            return
          //  self.alertControlling(title: "Network Error", message: "network Problem in you area")
            
            
        }
    
    }
    func  alertControlling(title: String, message: String)  {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let okAction = UIAlertAction(title: "Ok", style: .default) { (_) in
            alertController.dismiss(animated: true, completion: nil)
        }
        alertController.addAction(okAction)
        self.present(alertController, animated: true)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = true
    }
    
    
    @objc func openSignUpController(){
        
        if let viewController = self.storyboard?.instantiateViewController(withIdentifier: "ViewController") as? ViewController {
            self.navigationController?.pushViewController(viewController, animated: true)
        }
        
    }
   
}

extension UITextField{
    
    func addRightView(){
        let btnView = UIButton(frame: CGRect(x: 0, y: 0, width: ((self.frame.height) * 0.90), height: ((self.frame.height) * 0.90)))
        btnView.setImage(#imageLiteral(resourceName: "view"), for: .normal)
        btnView.imageEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        self.rightViewMode = .always
        self.rightView = btnView
        
        btnView.addTarget(self, action: #selector(viewHide(sender:)), for: UIControl.Event.touchUpInside)
    }
    @objc
    func viewHide(sender: UIButton) {
        print(sender)
        if self.isSecureTextEntry{
            self.isSecureTextEntry = false
            sender.setImage(#imageLiteral(resourceName: "hide"), for: .normal)
            return
        }
        self.isSecureTextEntry = true
        sender.setImage(#imageLiteral(resourceName: "view"), for: .normal)

    }
    
}

extension LoginViewController: UIPickerViewDelegate, UIPickerViewDataSource{
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return countriesCode.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return countriesName[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        countryCodeTextField.text = countriesCode[row]
    }
}


extension UIViewController {
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIViewController.dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
}

extension LoginViewController : UITextFieldDelegate{
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        let newLength: Int = textField.text?.count ?? 0 + string.count - range.length
        let numberOnly = NSCharacterSet.init(charactersIn: ACCEPTABLE_NUMBERS).inverted
        let strValid = string.rangeOfCharacter(from: numberOnly) == nil
        
        if let char = string.cString(using: String.Encoding.utf8) {
            let isBackSpace = strcmp(char, "\\b")
            if (isBackSpace == -92) {
                return true
            }
        }
        
        
        return (strValid && (newLength < self.MAX_LENGTH_PHONENUMBER))
    }
}
