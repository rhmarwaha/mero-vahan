//
//  LeftMenuViewController.swift
//  MeroVahanApp
//
//  Created by Rohit Marwaha on 29/10/19.
//  Copyright © 2019 Rohit Marwaha. All rights reserved.
//

import UIKit

class LeftMenuViewController: UIViewController {

    @IBOutlet weak var profileImage: UIImageView!
    @IBOutlet weak var tableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        loadAccessories()
        loadTableView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = true
        tableView.tableFooterView = UIView()
    }
    

    func loadAccessories(){
        
        profileImage.layer.cornerRadius = 25.0
    }
    
    func loadTableView(){
        let nibCell = UINib(nibName: "LeftMenuTableViewCell", bundle: Bundle.main)
        tableView.register(nibCell, forCellReuseIdentifier: "LeftMenuTableViewCell")
        tableView.delegate = self
        tableView.dataSource = self
    }

}


extension LeftMenuViewController: UITableViewDelegate, UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 5
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "LeftMenuTableViewCell") as? LeftMenuTableViewCell else{
            fatalError("Cell does not load")
        }
        let rowType = LeftMenuCellEnum(rawValue: indexPath.row)
        cell.label.text = rowType?.title
        return cell
        
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
      //  tableView.deselectRow(at: indexPath, animated: true)
         //let cell  = tableView.cellForRow(at: indexPath) as? LeftMenuTableViewCell
         //cell?.sideView.isHidden = false
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50.0
    }
}


enum LeftMenuCellEnum: Int{
    case home = 0
    case profile
    case payement
    case contact
    case logout
    
    var title: String{
        switch self{
        case .home:
            return "Home"
        case .profile:
            return "Profile"
        case .payement:
            return "Payement"
        case .contact:
            return "Contact"
        case .logout:
            return "Logout"
        }
    }
}
