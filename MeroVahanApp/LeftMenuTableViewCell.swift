//
//  LeftMenuTableViewCell.swift
//  MeroVahanApp
//
//  Created by Rohit Marwaha on 29/10/19.
//  Copyright © 2019 Rohit Marwaha. All rights reserved.
//

import UIKit

class LeftMenuTableViewCell: UITableViewCell {

    @IBOutlet weak var sideView: UIView!
    
    @IBOutlet weak var label: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        let selectedView = UINib(nibName: "SelectedCellNib", bundle: Bundle.main).instantiate(withOwner: self, options: nil).first as? UIView        //selectedView.backgroundColor = .red
        selectedBackgroundView = selectedView
       
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
