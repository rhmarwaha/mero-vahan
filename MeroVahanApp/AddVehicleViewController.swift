//
//  AddVehicleViewController.swift
//  MeroVahanApp
//
//  Created by Rohit Marwaha on 31/10/19.
//  Copyright © 2019 Rohit Marwaha. All rights reserved.
//

import UIKit

class AddVehicleViewController: UIViewController {

    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var contactAddressTextField: UITextField!
    @IBOutlet weak var citizenshipTextField: UITextField!
    @IBOutlet weak var panNumberTextField: UITextField!
    @IBOutlet weak var vehicleNumberTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var rcNumberTextField: UITextField!
    @IBOutlet weak var chasisNumberTextField: UITextField!
    @IBOutlet weak var vehicleModelYearTextField: UITextField!
    @IBOutlet weak var vehicleTypeTextField: UITextField!
    @IBOutlet weak var numberOfTyresTextField: UITextField!
    @IBOutlet weak var capacityTextField: UITextField!
    
    @IBOutlet weak var numberOfTyresButton: UIButton!
    @IBOutlet weak var capacityButton: UIButton!
    
    @IBOutlet weak var addVehicleButton: UIButton!
    
    let datePicker = UIDatePicker()
    let vehicleTypePickerView = UIPickerView()
    let numberOfTyresPickerView = UIPickerView()
    let capacityPickerView = UIPickerView()
    
    var vehicleType: [String] = [String]()
    var numberOfTyresArrary: [Int] = [Int]()
    var capacityArray: [Int] = [Int]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        
        let yourBackImage = #imageLiteral(resourceName: "leftarrow")
        self.navigationController?.navigationBar.backIndicatorImage = yourBackImage
        self.navigationController?.navigationBar.backIndicatorTransitionMaskImage = yourBackImage
        self.navigationItem.title = ""
        
        self.navigationController?.navigationBar.isHidden = false
        self.navigationController?.navigationBar.barTintColor = UIColor(named: "lightThemeColor")
        self.navigationController?.navigationBar.tintColor = .white
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor : UIColor.white]
        self.title = "Add Vehicle"
        
        addAccessories()
    }
    
    func addAccessories(){
        
        addVehicleButton.layer.shadowColor = UIColor.black.cgColor
        addVehicleButton.layer.shadowOffset = CGSize(width: 10, height: 10)
        addVehicleButton.layer.shadowRadius = 5
        addVehicleButton.layer.shadowOpacity = 0.2
        
        nameTextField.delegate = self
        vehicleNumberTextField.delegate = self
        chasisNumberTextField.delegate = self
        
        datePicker.datePickerMode = .date
        datePicker.minimumDate = Calendar.current.date(byAdding: Calendar.Component.year, value: -20, to: Date())
        datePicker.maximumDate = Calendar.current.date(byAdding: Calendar.Component.year , value: 0, to: Date())
        datePicker.addTarget(self, action: #selector(returnDataValue), for: UIControl.Event.valueChanged)
        vehicleModelYearTextField.inputView = datePicker
        
        vehicleTypeTextField.inputView = vehicleTypePickerView
        vehicleTypePickerView.delegate = self
        vehicleTypePickerView.dataSource = self
        
        numberOfTyresTextField.inputView = numberOfTyresPickerView
        numberOfTyresPickerView.delegate = self
        numberOfTyresPickerView.dataSource = self
        
        capacityTextField.inputView = capacityPickerView
        capacityPickerView.delegate = self
        capacityPickerView.dataSource = self
        
        vehicleTypeTextField.addTarget(self, action: #selector(textFieldIsNotEmpty(sender:)), for: .editingDidEnd)
        numberOfTyresTextField.addTarget(self, action: #selector(textFieldIsNotEmpty(sender:)), for: .editingDidEnd)
    }
    
    @objc
    func textFieldIsNotEmpty(sender: UITextField){
        if sender == self.vehicleTypeTextField{
            let textValue = sender.text ?? ""
            if !(textValue.isEmpty){
                numberOfTyresTextField.isUserInteractionEnabled = true
                numberOfTyresButton.isUserInteractionEnabled = true
            }
            return
        }
        if sender == self.numberOfTyresTextField{
            let textValue = sender.text ?? ""
            if !(textValue.isEmpty){
                capacityTextField.isUserInteractionEnabled = true
                capacityButton.isUserInteractionEnabled = true
                capacityButton.sendActions(for: .touchUpInside)
            }
        }
        
    }
    
    @IBAction func numberOfTyresAction(_ sender: Any) {
        let vehicleType = vehicleTypeTextField.text ?? ""
        ApiHit.getNumberOfTyresOfVehicle(vehicleType: vehicleType) { (isSuccess, response) in
            if isSuccess{
                guard let responseValue = response as? [String : Any] else{
                    return
                }
                print(responseValue)
                guard let statusCode = responseValue["statusCode"] as? Int else{
                    return
                }
                print(statusCode)
                
                guard let data = responseValue["data"] as? [String: Any] else{
                    return
                }
                guard let vehicleDetails = data["vehicleDetails"] as? [Int] else{
                    return
                }
                print(vehicleDetails)
                self.numberOfTyresArrary = vehicleDetails
                self.numberOfTyresTextField.becomeFirstResponder()
                self.numberOfTyresTextField.text = String(self.numberOfTyresArrary[0])
                return
            }
            self.alertControlling(title: "Something Went Wrong", message: "Network problrm may be ....")
        }
    }
    
    @IBAction func capacityAction(_ sender: Any) {
        let vehicleType = vehicleTypeTextField.text ?? ""
        let numberOfTyres = Int(numberOfTyresTextField.text!) ?? 0
        
        ApiHit.getCapacityOfVehicle(vehicleType: vehicleType, numberOfTyres: numberOfTyres) { (isSuccess, response) in
            
            if isSuccess{
                guard let responseValue = response as? [String : Any] else{
                    return
                }
                print(responseValue)
                guard let statusCode = responseValue["statusCode"] as? Int else{
                    return
                }
                print(statusCode)
                
                guard let data = responseValue["data"] as? [String: Any] else{
                    return
                }
                guard let vehicleDetails = data["vehicleDetails"] as? [Int] else{
                    return
                }
                print(vehicleDetails)
                self.capacityArray = vehicleDetails
                self.capacityTextField.becomeFirstResponder()
                self.capacityTextField.text = String(self.capacityArray[0])
                return
            }
            self.alertControlling(title: "Something Went Wrong", message: "Network problrm may be ....")
            
        }
        
    }
    @IBAction func vehicleType(_ sender: Any) {
        ApiHit.getVehicleList { (isSuccess, response) in
            if isSuccess{
                guard let responseValue = response as? [String : Any] else{
                    return
                }
                
                print(responseValue)
                
                
                guard let statusCode = responseValue["statusCode"] as? Int else{
                    return
                }
                print(statusCode)
                guard let message = responseValue["message"] as? String else{
                    return
                }
                if statusCode == 200{
                    guard let data = responseValue["data"] as? [String: Any] else{
                        return
                    }
                    guard let vehicleDetails = data["vehicleDetails"] as? [String] else{
                        return
                    }
                    print(vehicleDetails)
                    self.vehicleType = vehicleDetails
                    self.vehicleTypeTextField.becomeFirstResponder()
                    self.vehicleTypeTextField.text = self.vehicleType[0]
                    return
                }
                
                self.alertControlling(title: "Something Went Wrong", message: message)
            }
        }
    }
    func  alertControlling(title: String, message: String)  {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let okAction = UIAlertAction(title: "Ok", style: .default) { (_) in
            alertController.dismiss(animated: true, completion: nil)
        }
        alertController.addAction(okAction)
        self.present(alertController, animated: true)
    }
    
    @objc func returnDataValue(){
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy"
        vehicleModelYearTextField.text = dateFormatter.string(from: datePicker.date)
        
    }
    
    @IBAction func vehicleModelYearAction(_ sender: Any) {
            vehicleModelYearTextField.becomeFirstResponder()
    }
    
    @IBAction func addVehicleButtonAction(_ sender: Any) {
        let nameValue = nameTextField.text ?? ""
        let contactAddressValue = contactAddressTextField.text ?? ""
        let citizenshipvalue = citizenshipTextField.text ?? ""
        let panValue = panNumberTextField.text ?? ""
        let vehicleNumberValue = vehicleNumberTextField.text ?? ""
        let passwordValue = passwordTextField.text ?? ""
        let rcNumbervalue = rcNumberTextField.text ?? ""
        let chasisNumberValue = chasisNumberTextField.text ?? ""
        let vehicleModelYearValue = vehicleModelYearTextField.text ?? ""
        let vehicleTypeValue = vehicleTypeTextField.text ?? ""
        let numberOfTyresValue = numberOfTyresTextField.text ?? ""
        let capacityValue = capacityTextField.text ?? ""
        
        if nameValue.isEmpty{
            alertControlling(title: "Name Warning", message: "Name is Missing .....")
        }
        if contactAddressValue.isEmpty{
            alertControlling(title: "Address Warning", message: "Address is missing .....")
        }
        if citizenshipvalue.isEmpty{
            alertControlling(title: "Citizenship warning ", message: "Citizen Ship number is missing .....")
        }
        if vehicleNumberValue.isEmpty{
            alertControlling(title: "Vehicle Number Warning ", message: "Vehicle Number is missing ....")
        }
        if passwordValue.isEmpty || !passwordValue.isValidPassword(){
            alertControlling(title: "Not a valid password ", message: "Password must contain a symbol , a capital character and a number .....")
        }
        if rcNumbervalue.isEmpty{
            alertControlling(title: "RC Number Warning", message: "RC Number is missing .....")
        }
        if chasisNumberValue.isEmpty{
            alertControlling(title: "Chasis Number Warning", message: "Chasis Number is missing .....")
        }
        if vehicleModelYearValue.isEmpty{
            alertControlling(title: "Vehicle model year Warning", message: "Vehicle model year is missing .....")
        }
        if vehicleTypeValue.isEmpty{
            alertControlling(title: "Vehicle Type Warning", message: "Vehicle Type is missing ......")
        }
        if numberOfTyresValue.isEmpty{
            alertControlling(title: "Tyres Warning", message: "Number of tyres as missing .....")
        }
        if capacityValue.isEmpty{
            alertControlling(title: "Capacity Warning ", message: "Vehicle Capacity is missing ......")
        }
        let vehicle = Vehicle(name: nameValue, contactAdress: contactAddressValue, citizenShipNumber: citizenshipvalue, plateNumber: panValue, rcNumber: rcNumbervalue, chasisNumber: chasisNumberValue, vehicleModelYear: vehicleModelYearValue, vehicleType: vehicleTypeValue, numberOfTyres: Double(numberOfTyresValue)!, capacity: Double(capacityValue)! , password: passwordValue)
        
        ApiHit.addVehicle(vehicle: vehicle) { (isSuccess, response) in
            if isSuccess{
                guard let responseValue = response as? [String : Any] else{
                    return
                }
                print(responseValue)
                guard let statusCode = responseValue["statusCode"] as? Int else{
                    return
                }
                print(statusCode)
               
                guard let data = responseValue["data"] as? [String: Any] else{
                    return
                }
                if statusCode == 200{
                    self.alertControlling(title: "Added Vehicle", message: "Vehicle is added successfully .....")
                }
                
                return
            }
            self.alertControlling(title: "Something Went Wrong", message: "Network problrm may be ....")
        }
    }
}

extension AddVehicleViewController: UITextFieldDelegate{
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if textField == self.vehicleNumberTextField{
            
            let allowedCharacters = CharacterSet(charactersIn: " ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789")
            let characterSet = CharacterSet(charactersIn: string)
            return allowedCharacters.isSuperset(of: characterSet)

        }
        
        if textField == self.chasisNumberTextField{
            let allowedCharacters = CharacterSet(charactersIn: "0123456789")
            let characterSet = CharacterSet(charactersIn: string)
            return allowedCharacters.isSuperset(of: characterSet)
        }
        
        let allowedCharacters = CharacterSet.letters
        let characterSet = CharacterSet(charactersIn: string)
        return allowedCharacters.isSuperset(of: characterSet)
    }
}

extension AddVehicleViewController: UIPickerViewDelegate, UIPickerViewDataSource{
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if pickerView == self.numberOfTyresPickerView{
            return numberOfTyresArrary.count
        }
        if pickerView == self.capacityPickerView{
            return capacityArray.count
        }
        return vehicleType.count
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if pickerView == self.numberOfTyresPickerView{
            numberOfTyresTextField.text = String(numberOfTyresArrary[row])
            capacityButton.sendActions(for: .touchUpInside)
            return
        }
        if pickerView == self.capacityPickerView{
            capacityTextField.text = String(capacityArray[row])
            return
        }
        vehicleTypeTextField.text = vehicleType[row]
        numberOfTyresButton.sendActions(for: .touchUpInside)
        
        
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        if pickerView == self.numberOfTyresPickerView{
            return String(numberOfTyresArrary[row])
        }
        if pickerView == self.capacityPickerView{
            return String(capacityArray[row])
        }
        return vehicleType[row]
    }
}
