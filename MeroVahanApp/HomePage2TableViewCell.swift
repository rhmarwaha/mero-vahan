//
//  HomePage2TableViewCell.swift
//  MeroVahanApp
//
//  Created by Rohit Marwaha on 25/10/19.
//  Copyright © 2019 Rohit Marwaha. All rights reserved.
//

import UIKit

class HomePage2TableViewCell: UITableViewCell {
    @IBOutlet weak var cellView: UIView!
    @IBOutlet weak var imageCell: UIImageView!
    @IBOutlet weak var cellName: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setUpView()
    }

    
    func setUpView(){
        
        imageCell.layer.cornerRadius = 50.0
        imageCell.layer.shadowColor = UIColor.blue.cgColor
        imageCell.layer.shadowOffset = CGSize(width: 50, height: 50)
        imageCell.layer.shadowRadius = 5
        imageCell.layer.shadowOpacity = 1
        
        
        cellView.layer.shadowColor = UIColor.black.cgColor
        cellView.layer.shadowOffset = CGSize(width: 10, height: 10)
        cellView.layer.shadowRadius = 5
        cellView.layer.shadowOpacity = 0.1
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
