//
//  TermsConditionViewController.swift
//  MeroVahanApp
//
//  Created by Rohit Marwaha on 23/10/19.
//  Copyright © 2019 Rohit Marwaha. All rights reserved.
//

import UIKit

class TermsConditionViewController: UIViewController {

    @IBOutlet weak var webView: UIWebView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Terms and Conditions"
        self.navigationController?.navigationBar.backItem?.title = ""
        loadWebAddress()

    }
    
    func loadWebAddress(){
        guard let url = URL(string: "https://www.google.com") else {
            fatalError("Wrong in built url ")
        }
        let myRequest = URLRequest(url: url)
        webView.loadRequest(myRequest)
        
    }
}
