//
//  SelectedCellNib.swift
//  MeroVahanApp
//
//  Created by Rohit Marwaha on 29/10/19.
//  Copyright © 2019 Rohit Marwaha. All rights reserved.
//

import UIKit

class SelectedCellNib: UIView {

    @IBOutlet var contentView: SelectedCellNib!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    private func commonInit(){
//
//        Bundle.main.loadNibNamed("SelectedCellNib", owner: self, options: nil)
//        addSubview(contentView)
//        contentView.frame = self.bounds
//        contentView.autoresizingMask = [.flexibleHeight, .flexibleWidth]
    }
}
