//
//  Vehicle.swift
//  MeroVahanApp
//
//  Created by Rohit Marwaha on 01/11/19.
//  Copyright © 2019 Rohit Marwaha. All rights reserved.
//

import Foundation

class Vehicle{
    var name: String
    var contactAdress: String
    var citizenShipNumber: String
    var plateNumber: String
    var rcNumber: String
    var chasisNumber: String
    var vehicleModelYear: String
    var vehicleType: String
    var numberOfTyres: Double
    var capacity: Double
    var password: String

    init(name: String, contactAdress: String, citizenShipNumber: String, plateNumber: String, rcNumber: String, chasisNumber: String, vehicleModelYear: String, vehicleType: String, numberOfTyres: Double, capacity: Double, password: String) {
        self.name = name
        self.contactAdress = contactAdress
        self.citizenShipNumber = citizenShipNumber
        self.plateNumber = plateNumber
        self.rcNumber = rcNumber
        self.chasisNumber = chasisNumber
        self.vehicleModelYear = vehicleModelYear
        self.vehicleType = vehicleType
        self.numberOfTyres = numberOfTyres
        self.capacity = capacity
        self.password = password
    }

}
