//
//  User.swift
//  MeroVahanApp
//
//  Created by Rohit Marwaha on 24/10/19.
//  Copyright © 2019 Rohit Marwaha. All rights reserved.
//

import Foundation
import Alamofire

typealias APICallBack = ((_ issuccess: Bool, _ response: Any?) -> Void)

class User {
    
    var firstName: String
    var lastName: String
    var emailAddress: String
    var countryCode: String
    var mobileNumber: String
    var password: String
    
    init(firstName: String, lastName: String, emailAddress: String, countryCode: String, mobileNumber: String, password: String) {
        self.firstName = firstName
        self.lastName = lastName
        self.emailAddress = emailAddress
        self.countryCode = countryCode
        self.mobileNumber = mobileNumber
        self.password = password
    }
    
    
}


class ApiHit{
    static func signUpApiHit(user: User, completion: @escaping APICallBack){
        var param = [String : Any]()
        param = [
            "password" : user.password,
            "mobile" : user.mobileNumber,
            "name" : user.firstName,
            "lastName": user.lastName,
            "deviceType" : Constants.deviceType,
            "countryCode" : user.countryCode,
            "deviceToken" : Constants.deviceToken
        ]
        var headers = [String: String]()
        headers = ["content-language": Constants.contentLanguage , "utcoffset" : Constants.utcoffset ]
        
        guard let url = URL(string: "\(API.signUpUrl)") else {
            completion(false, nil)
            return
        }
        Alamofire.request(url, method: .post, parameters: param, encoding: URLEncoding.default, headers: headers).responseJSON { (response) in
            
            guard response.result.isSuccess else{
                completion(false, nil)
                return
            }
            guard let value = response.result.value else{
                completion(false , nil)
                return
            }
            completion(true,value)
        }
    }
    
    static func loginApiHit(countryCode: String, mobile: String, password: String, completion: @escaping APICallBack){
        var param = [String : Any]()
        param = [
            "mobile" : mobile,
            "password" : password,
            "appVersion" : Constants.appVersion,
            "deviceType" : Constants.deviceType,
            "countryCode" : countryCode,
            "role" : Constants.role ,
            "deviceToken" : Constants.deviceToken
        ]
        var headers = [String: String]()
        headers = ["content-language": Constants.contentLanguage , "utcoffset" : Constants.utcoffset ]
        
        guard let url = URL(string: "\(API.loginUrl)") else {
            completion(false, nil)
            return
        }
        Alamofire.request(url, method: .post, parameters: param, encoding: URLEncoding.default, headers: headers).responseJSON { (response) in
            
            guard response.result.isSuccess else{
                
                completion(false, nil)
                return
            }
            guard let value = response.result.value else{
                completion(false , nil)
                return
            }
            completion(true,value)
        }
    }
    static func getVehicleList(completion: @escaping APICallBack){
        
        let token = UserDefaults.standard.string(forKey: UserDefaultsKeys.token) ?? ""
        
        var headers = [String: String]()
        headers = ["authorization": "bearer " + token, "content-language": Constants.contentLanguage  ]
        
        print(token)
        guard let url = URL(string: "\(API.vehicleList)") else{
            completion(false,nil)
            return
        }
       
        
        Alamofire.request(url, method: .get, parameters: nil, encoding: URLEncoding.default, headers: headers).responseJSON { (response) in
            
            guard response.result.isSuccess else{
                completion(false,nil)
                return
            }
            guard let value = response.result.value else{
                completion(false , nil)
                return
            }
            completion(true,value)
        }
    }
    
    static func getNumberOfTyresOfVehicle(vehicleType:String, completion: @escaping APICallBack){
        
        let token = UserDefaults.standard.string(forKey: UserDefaultsKeys.token) ?? ""
        
        var headers = [String: String]()
        headers = ["authorization": "bearer " + token, "content-language": Constants.contentLanguage  ]
        
        print(token)

        
      
        
        let urlString = "\(API.vehicleList)?vehicleType=\(vehicleType)".addingPercentEncoding(withAllowedCharacters: NSCharacterSet.urlQueryAllowed)!
        guard URL(string: urlString) != nil else{
            completion(false,nil)
            return
        }
        
        Alamofire.request(urlString, method: .get, parameters: nil, encoding: URLEncoding.queryString, headers: headers).responseJSON { (response) in
            
            guard response.result.isSuccess else{
                completion(false,nil)
                return
            }
            guard let value = response.result.value else{
                completion(false , nil)
                return
            }
            completion(true,value)
        }
    }
    
    static func getCapacityOfVehicle(vehicleType: String, numberOfTyres: Int, completion: @escaping APICallBack){
        
        let token = UserDefaults.standard.string(forKey: UserDefaultsKeys.token) ?? ""
        
        var headers = [String: String]()
        headers = ["authorization": "bearer " + token, "content-language": Constants.contentLanguage]
        
        let urlString = "\(API.vehicleList)?vehicleType=\(vehicleType)&numberOfTyres=\(numberOfTyres)".addingPercentEncoding(withAllowedCharacters: NSCharacterSet.urlQueryAllowed)!
        guard URL(string: urlString) != nil else{
            completion(false,nil)
            return
        }
        Alamofire.request(urlString, method: .get, parameters: nil, encoding: URLEncoding.queryString, headers: headers).responseJSON { (response) in
            
            guard response.result.isSuccess else{
                completion(false,nil)
                return
            }
            guard let value = response.result.value else{
                completion(false , nil)
                return
            }
            completion(true,value)
        }
    }
    static func addVehicle(vehicle: Vehicle, completion: @escaping APICallBack ){
        var param = [String : Any]()
        param = [
            "vehicleOwnerName": vehicle.name,
            "contactAddress": vehicle.contactAdress,
            "citizenOrFirmRegNo": vehicle.citizenShipNumber,
            "plateNumber": vehicle.plateNumber,
            "rcNumber": vehicle.rcNumber,
            "chasisNumber": vehicle.chasisNumber,
            "vehicleModelYear": vehicle.vehicleModelYear,
            "vehicleType": vehicle.vehicleType,
            "numberOfTyres": vehicle.numberOfTyres,
            "weightCapacity": vehicle.capacity,
            "password": vehicle.password
        ]
        let token = UserDefaults.standard.string(forKey: UserDefaultsKeys.token) ?? ""
        var headers = [String: String]()
        headers = ["authorization": "bearer " + token, "content-language": Constants.contentLanguage]
       
        guard let url = URL(string: "\(API.addVehicle)") else {
            completion(false, nil)
            return
        }
        Alamofire.request(url, method: .post, parameters: param, encoding: URLEncoding.default, headers: headers).responseJSON { (response) in
            
            guard response.result.isSuccess else{
                completion(false, nil)
                return
            }
            guard let value = response.result.value else{
                completion(false , nil)
                return
            }
            completion(true,value)
        }
    }
}

struct Constants{
    static let appVersion = "100"
    static let role = "fleetOwner"
    static var deviceToken: String = "123456"
    static var deviceType: String = "IOS"
    static var contentLanguage = "en"
    static var utcoffset = "-330"
}

struct API {
    static let loginUrl = "http:/13.232.75.44:3001/user/login"
    static let signUpUrl = "http:/13.232.75.44:3001/fleetOwner/register"
    static let vehicleList = "http://13.232.75.44:3001/fleetOwner/getVehicleList"
    static let addVehicle = "http://13.232.75.44:3001/fleetOwner/addVehicle"
}

struct UserDefaultsKeys{
    static let token = "token"
}
