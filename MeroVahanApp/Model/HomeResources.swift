//
//  HomeResources.swift
//  MeroVahanApp
//
//  Created by Rohit Marwaha on 25/10/19.
//  Copyright © 2019 Rohit Marwaha. All rights reserved.
//

import Foundation
import UIKit

class HomeResources{
    var name: String
    var image: UIImage
    
    init(name: String, image: UIImage) {
        self.name = name
        self.image = image
    }
}
